Place these scripts in `/data/data/com.termux/files/home/.shortcuts`
so that they get detected by https://f-droid.org/packages/com.termux.widget/

Depends on having these three apps installed on the android system:
https://f-droid.org/packages/com.termux.widget/
https://f-droid.org/packages/com.termux.api/
https://f-droid.org/packages/com.termux/

And depends on having these apps installed as termux packages:
python pip 

And also!!! depends on having installed youtube-dl and bandcamp-dl with pip
(depending on the script in question)
