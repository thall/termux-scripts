#!/bin/bash
pl=$(termux-dialog -i "playlist-url" -t "youtube-dl")
cmd="youtube-dl -x --yes-playlist $pl"
echo "$pl"
$cmd
termux-toast "youtube download finished"
