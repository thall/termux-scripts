#!/bin/bash
album=$(termux-dialog -i "album-url" -t "bandcamp-dl")
cmd="bandcamp-dl $album"
echo "$cmd"
$cmd
termux-toast "bandcamp download finished"
